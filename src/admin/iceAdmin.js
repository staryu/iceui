/**
 +------------------------------------------------------------------------------------+
 + 后台
 +------------------------------------------------------------------------------------+
 + 作者：ice
 + 官方：www.uzjz.com
 + 时间：2018-04-25
 +------------------------------------------------------------------------------------+
 */

ice(function(){

	//侧栏隐藏展开-收缩
	ice('.admin-toggle')[0].onclick=function(){
		ice(this).toggleCss('open','close');
		ice('.admin-page').toggleCss('admin-sidebar-close');
		ice('.admin-sidebar').toggleCss('open','close');
	}

	//侧栏菜单隐藏展开-收缩
	ice('.admin-toggle-right')[0].onclick=function(){
		ice('.admin-menu').toggleCss('open','close');
		ice.toggleCss(this,'open');
	}

	//监听侧栏菜单
	ice('.admin-sidebar-menu a').click(function(){
		//移动端收缩菜单
		if(ice.web().w <= 768){
			ice('.admin-toggle').toggleCss('open','close');
			ice('.admin-page').toggleCss('admin-sidebar-close');
			ice('.admin-sidebar').toggleCss('open','close');
		}
		return false;
	});

	//侧栏菜单展开-收缩
	ice.tree({
		el:'#adminTree',
	});
	
	var moveLine = ice('.admin-menu-move');
	//菜单树高亮
	setTimeout(function(){
		var nav = ice('.admin-sidebar-menu a');
		nav.each(function(){
			if(this.href == document.location.origin + document.location.pathname){
				ice(this).addCss('active');
				var ul = this.parentNode.parentNode;
				if(ice(ul).hasCss('admin-menu-dropdown')){
					ice(ul).delCss('tree-close').addCss('tree-open').show().parent().delCss('tree-close').addCss('tree-open')
				}
				moveLine.css('top',ice(this.parentNode).page().y-ice.scroll('y')+'px');
			}else{
				ice(this).delCss('active');
			}
		});
		nav.on('mouseover',function(){
			moveLine.css('top',ice(this).page().y-ice.scroll('y')+'px');
		})
		nav.on('mouseout',function(){
			moveLine.css('top',ice('.admin-sidebar-menu .active').page().y+'px');
		})
	},300);
});